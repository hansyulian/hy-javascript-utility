var HY = (function (HY) {
    //loop from the end until the start
    HY.Array.eachFromEnd = function (arr, callback) {
        for (var i = arr.length - 1; i >= 0; i--)
            callback(arr[i]);
    };
    return HY;
})(HY);