var HY = (function(HY) {
    //loop from the start until the end
    HY.Array.eachFromStart = function(arr, callback) {
        for (var i = 0; i < arr.length; i++)
            callback(arr[i]);
    };
    return HY;
})(HY);
