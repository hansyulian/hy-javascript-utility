(function (HY) {

	HY.Utility.copy = function (object) {
		return JSON.parse(JSON.stringify(object));
	};
	return HY;
})(HY);