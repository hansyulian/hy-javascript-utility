(function (HY) {

    HY.Color.shiftHexadecimal = function (color, value) {
        var length = color.length;
        if (length != 4 && length != 7)
            return null;
        var width = parseInt(length / 3);
        var result = "#";
        var maxValue = 1;
        for (var i = 0; i < width; i++)
            maxValue *= 16;
        for (var i = 0; i < 3; i++) {
            var channelCode = color.substring(i * width + 1, (i + 1) * width + 1);
            var channelValue = parseInt(channelCode, 16);
            var shiftedValue = channelValue * (1 + value);
            if (shiftedValue >= maxValue)
                shiftedValue = maxValue - 1;
            if (shiftedValue < 0)
                shiftedValue = 0;
            shiftedValue = parseInt(shiftedValue);
            result += shiftedValue.toString(16);
        }
        return result;
    };
    return HY;
})(HY);