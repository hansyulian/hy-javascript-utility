var HY = (function (HY) {
    function Pipeline() {
        var args = arguments;
        return (function (args) {
            var pipeline = {
                state: "waiting",
                context: {},
                processQueue: [],
                lastProcessIndex: 0,
                then: function () {
                    var nextProcess = arguments;
                    for (var i in nextProcess)
                        addProcess(nextProcess[i]);
                    executeProcessQueue();
                    return this;
                },
                restart: function () {
                    restartPipeline();
                }
            }

            for (var i in args)
                addProcess(args[i]);

            restartPipeline();
            return pipeline;

            function addProcess(process) {
                if (process) {
                    pipeline.processQueue.push(process);
                }
            }

            function restartPipeline() {
                pipeline.lastProcessIndex = 0;
                pipeline.state = "ready";
                executeProcessQueue();
            }

            function nextCaller() {
                pipeline.state = "ready";
                pipeline.lastProcessIndex++;
                executeProcessQueue();
            }
            //callback structure is function(args,context,next)

            function executeProcessQueue() {
                if (pipeline.state == "ready" && pipeline.lastProcessIndex < pipeline.processQueue.length) {
                    var callback = pipeline.processQueue[pipeline.lastProcessIndex];
                    pipeline.state == "executing";
                    callback(pipeline.context, nextCaller, failed);
                }
            }

            function failed() {
                pipeline.state = "failed";
            }
        })(args);
    }
    HY.Class.Pipeline = Pipeline;
    return HY;
    /*
        var pipeline = Pipeline(f1,f2,f3);
        var pipeline.then(f4);
        var pipeline.then(f5,f6)

        var f1 = f2 = f3 = f4 = f5 = f6 = function(context, next){
            context.value = context.value + 1 || 0;
            console.log(context.value);
            next();
        }
    */
})(HY);