describe("Object: Get", function () {
    var HY = require("../../../dist/hy-node");
    it("simple get", function () {
        var myObject = {
            comeHere: {
                andHere: [{
                    name: 'correct'
                }, {
                    name: 'wrong'
                }]
            },
            notHere: {

            }
        }
        expect(HY.Object.get(myObject, "comeHere.andHere.0.name")).toEqual("correct");
    })
    it("simple get fail", function () {
        var myObject = {
            comeHere: {
                andHere: [{
                    name: 'correct'
                }, {
                    name: 'wrong'
                }]
            },
            notHere: {

            }
        }
        expect(HY.Object.get(myObject, "notHere.andHere.0.name")).toEqual(undefined);
    })
});