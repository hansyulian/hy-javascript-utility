describe("Math: Scale", function () {
    var HY = require("../../../dist/hy-node");
    it("simple scale", function () {
        expect(HY.Math.scale(35, 0, 100)).toEqual(0.35);
    })
});