describe("String: Format", function () {
	var HY = require("../../../dist/hy-node");
	it("simple format", function () {
		var string = "Test {0} {1} {0}";
		expect(HY.String.format(string, "counter 1", "counter 2")).toEqual("Test counter 1 counter 2 counter 1");
	})
});