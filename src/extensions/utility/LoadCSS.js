(function (HY) {

    HY.Utility.LoadCSS = function (url) {
        var fileref = document.createElement("link")
        fileref.setAttribute("rel", "stylesheet")
        fileref.setAttribute("type", "text/css")
        fileref.setAttribute("href", url)
    };
    return HY;
})(HY);