describe("Utility: Compare", function () {
    var HY = require("../../../dist/hy-node");
    it("simple compare", function () {
        var object1 = {
            a: 1,
            b: 2,
            c: 3,
        };
        var object2 = {
            a: 1,
            b: 2,
            c: 4,
        }
        var result = {
            a: [1, 1],
            b: [2, 2],
            c: [3, 4],
            _difference: {
                c: [3, 4]
            }
        }
        expect(HY.Utility.compare([object1, object2])).toEqual(result);
    })
});