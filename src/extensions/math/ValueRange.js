var HY = (function(HY) {

    HY.Math.valueRange = function(value, min, max, ranges) {
        var scaledValue = HY.Math.scale(value, min, max);
        for (var i = 0; i < ranges.length; i++)
            if (ranges[i].from <= scaledValue && ranges[i].to >= scaledValue)
                return ranges[i].value;
        return null;
    };
    return HY;
})(HY);