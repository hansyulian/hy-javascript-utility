var HY = (function (HY) {

	HY.Utility.addCSS = function (selector, rule) {
		var stylesheet = document.styleSheets[0];

		if (stylesheet.insertRule) {
			stylesheet.insertRule(selector + rule, stylesheet.cssRules.length);
		} else if (stylesheet.addRule) {
			stylesheet.addRule(selector, rule, -1);
		}
	};
	return HY;
})(HY);