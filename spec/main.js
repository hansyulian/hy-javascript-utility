describe("Main Module Test", function () {
	HY = require("../dist/hy-node");
	it("expect HY to be defined", function () {
		expect(HY).toBeDefined();
	});
	it("expect version to be defined", function () {
		expect(HY.version).toBeDefined();
	})
})